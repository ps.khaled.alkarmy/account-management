package com.progressoft.application;

import com.github.javafaker.Faker;
import net.kaczmarzyk.spring.data.jpa.web.SpecificationArgumentResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import usecases.CreateAccountUseCase;

import java.util.List;

@SpringBootApplication
public class Application implements CommandLineRunner , WebMvcConfigurer{
    private final CreateAccountUseCase createAccountUseCase;

    public Application(CreateAccountUseCase createAccountUseCase) {
        this.createAccountUseCase = createAccountUseCase;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new SpecificationArgumentResolver());
    }

    @Override
    public void run(String... args) throws Exception {
        Faker faker = new Faker();
        String[] customers = {"KHALEDKAR" , "YOUSEFSUL" , "TAYSEERSAB"};

//        for (int i = 0; i < 10000; i++) {
//            String randomCustomer = customers[faker.number().numberBetween(0,2)];
//            Account account = Account.builder()
//                    .customerId(randomCustomer)
//                    .availableBalance(new BigDecimal(faker.number().numberBetween(1 , 9999999999L)))
//                    .build();
//            createAccountUseCase.execute(account);
//        }
    }
}
