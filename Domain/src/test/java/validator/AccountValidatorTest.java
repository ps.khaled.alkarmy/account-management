package validator;

import exception.InvalidAccountException;
import model.Account;
import model.Status;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;


@ExtendWith(MockitoExtension.class)
class AccountValidatorTest {

    @Mock
    private CustomerProvider customerProvider;

    @InjectMocks
    private AccountValidator accountValidator;

    @Test
    void givenInvalidAccount_whenValidate_thenExceptionIsThrown() {
        List<Violation> violations = accountValidator.validate(null);

        Assertions.assertEquals(violations.get(0).getException().getClass(), InvalidAccountException.class);

    }

//    @Test
//    public void givenValidAccount_whenValidate_thenExpectedResult() {
//        List<Violation> violations = accountValidator.validate(Account.builder()
//                .accountNumber(1333L)
//                .creationDate(LocalDateTime.now())
//                .availableBalance(BigDecimal.TEN)
//                .status(Status.Active)
//                .customerId("KHALEDKAR")
//                .id(1)
//                .build());
//
//        Assertions.assertTrue(violations.isEmpty());
//    }
}