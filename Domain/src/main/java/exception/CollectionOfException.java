package exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import validator.Violation;

import java.util.List;

@AllArgsConstructor
@Data
public class CollectionOfException extends RuntimeException{
    private final List<Violation> violations;
}
