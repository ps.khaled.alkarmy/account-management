package usecases;

import event.Event;
import event.EventPublisher;
import exception.CollectionOfException;
import lombok.AllArgsConstructor;
import model.Account;
import model.Status;
import org.apache.commons.math3.random.RandomDataGenerator;
import repository.AccountRepository;
import validator.AccountValidator;
import validator.Violation;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
public class CreateAccountUseCase {
    private final AccountRepository accountRepository;
    private final AccountValidator accountValidator;
    private final EventPublisher eventPublisher;

    public void execute(Account account) {
        List<Violation> violations = accountValidator.validate(account);
        if (violations.size() != 0) {
            throw new CollectionOfException(violations);
        }

        RandomDataGenerator randomDataGenerator = new RandomDataGenerator();
        long randomWithRandomDataGenerator = randomDataGenerator.nextLong(1_000_000_000_000_000L, 9_999_999_999_999_999L);
        account.setStatus(Status.Active);
        account.setAccountNumber(randomWithRandomDataGenerator);
        account.setCreationDate(LocalDateTime.now());

        accountRepository.save(account);

        //TODO This use case doesn't have to be a separate use case, just a private method
        publishEvent(account);
    }

    private void publishEvent(Object payload) {
        eventPublisher.publish(new Event(payload, "ACCOUNT CREATED"));
    }
}
