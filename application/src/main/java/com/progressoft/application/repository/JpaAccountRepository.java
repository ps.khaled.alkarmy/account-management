package com.progressoft.application.repository;

import com.progressoft.application.entity.AccountEntity;
import com.progressoft.application.resources.AccountResponse;
import com.progressoft.application.spec.CustomerIdAndAccountNumber;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Repository
public interface JpaAccountRepository
        extends JpaRepository<AccountEntity, String> , JpaSpecificationExecutor<AccountEntity> {

    Optional<AccountEntity> findByAccountNumberAndCustomerId(long accountNumber, String customerId);
}
