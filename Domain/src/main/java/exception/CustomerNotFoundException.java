package exception;

public class CustomerNotFoundException extends RuntimeException {

    String dd ="";

    public String getDd() {
        return dd;
    }

    public CustomerNotFoundException() {
        super();
    }

    public CustomerNotFoundException(String message) {
        super("Customer not found");
    }

    public CustomerNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
