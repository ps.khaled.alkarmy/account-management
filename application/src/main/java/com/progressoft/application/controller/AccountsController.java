package com.progressoft.application.controller;

import com.progressoft.application.entity.AccountEntity;
import com.progressoft.application.entity.AccountMapper;
import com.progressoft.application.repository.JpaAccountRepository;
import com.progressoft.application.resources.AccountRequest;
import com.progressoft.application.resources.AccountResponse;
import com.progressoft.application.spec.CustomerIdAndAccountNumber;
import exception.UserNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import model.Account;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.domain.GreaterThanOrEqual;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import repository.AccountRepository;
import usecases.CreateAccountUseCase;
import usecases.DeactivateAccountUseCase;
import usecases.InactivateAccountUseCase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/api/v1/accounts")
@AllArgsConstructor
public class AccountsController {

    //DONE Controllers or REST endpoints should not expose Entities or Domain models, Create resources/requests for them

    //DONE Use proper REST Conventions for URLs and HTTP Methods

    //TODO Lookup @RestControllerAdvice to handle exceptions and return proper HTTP Statuses

    // api/v1/accounts/createNewAccount X bad practice
    // /add X bad practice

    // /api/v1/accounts Post creates a new account
    // /api/v1/accounts?status=ACTIVE Get lists all accounts
    // /api/v1/accounts/activeAccounts Get lists all accounts
    // /api/v1/accounts/{customerId}/{accountNumber}

    private final CreateAccountUseCase createAccountUseCase;
    private final JpaAccountRepository jpaAccountRepository;
    private final DeactivateAccountUseCase deactivateAccountUseCase;

    private final AccountMapper accountMapper;
    private final InactivateAccountUseCase inactivateAccountUseCase;


    @GetMapping
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Map<String, Object>> getAllAccount(Pageable pageable,
                                                             @And({
                                                                     @Spec(path = "customerId", params = "customerId", spec = Like.class),
                                                                     @Spec(path = "availableBalance", params = "balance", spec = GreaterThanOrEqual.class),
                                                                     @Spec(path = "status" , params = "status" , spec = Equal.class)
                                                             })
                                                             Specification<AccountEntity> accountFilter) {
        Page<AccountResponse> data = jpaAccountRepository.findAll(accountFilter, pageable).map(accountMapper::toAccountResponse);

        Map<String, Object> response = new HashMap<>();
        response.put("currentPage", data.getNumber());
        response.put("totalPage", data.getTotalPages());
        response.put("totalElement", data.getTotalElements());
        response.put("accounts", data.getContent());

        log.info("Get all account {} , Current Page {}", data.getTotalElements(), data.getNumber());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody AccountRequest accountRequest) {
        Account map = accountMapper.toAccount(accountRequest);
        log.info("Create account  Customer ID {}", accountRequest.getCustomerId());

        createAccountUseCase.execute(map);
    }

    @PostMapping("{customerId}/{accountNumber}/deactivate")
    @CrossOrigin(origins = "http://localhost:4200")
    public void deactivate(@PathVariable String accountNumber, @PathVariable String customerId) {
        //TODO A bug will occur if the account is null

        Account account = jpaAccountRepository.findByAccountNumberAndCustomerId(Long.parseLong(accountNumber), customerId)
                .map(accountMapper::toAccount)
                .orElseThrow(UserNotFoundException::new);

        log.info("Deactivate Account number {}, Customer ID {}", account.getAccountNumber(), account.getCustomerId());
        deactivateAccountUseCase.execute(account);
    }

    @PostMapping("{customerId}/{accountNumber}/activate")
    @CrossOrigin(origins = "http://localhost:4200")
    public void activate(@PathVariable String accountNumber, @PathVariable String customerId) {
        //TODO A bug will occur if the account is null

        Account account = jpaAccountRepository.findByAccountNumberAndCustomerId(Long.parseLong(accountNumber), customerId)
                .map(accountMapper::toAccount)
                .orElseThrow(UserNotFoundException::new);

        log.info("Deactivate Account number {}, Customer ID {}", account.getAccountNumber(), account.getCustomerId());
        inactivateAccountUseCase.execute(account);
    }

    @ResponseBody
    @GetMapping(value = "{customerId}/{accountNumber}")
    @CrossOrigin(origins = "http://localhost:4200")
    public AccountResponse getAccountByCustomerIdAndAccountNumber(@PathVariable String accountNumber, @PathVariable String customerId) {
        AccountResponse accountResponse = jpaAccountRepository.findByAccountNumberAndCustomerId(Long.parseLong(accountNumber), customerId)
                .map(accountMapper::toAccountResponse)
                .orElseThrow(UserNotFoundException::new);

        log.info("Get Account by Account number {} and Customer ID {}", accountResponse.getAccountNumber(), customerId);
        return accountResponse;
    }

    @GetMapping("/test")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Map<String, Object>> getAllAccountSpec(CustomerIdAndAccountNumber customerIdAndAccountNumber) {
        List<AccountEntity> data = jpaAccountRepository.findAll(customerIdAndAccountNumber);

        Map<String, Object> response = new HashMap<>();
        response.put("accounts", data);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/account")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Map<String, Object>> getAccount(CustomerIdAndAccountNumber customerIdAndAccountNumber) {
        Optional<AccountEntity> account = jpaAccountRepository.findOne(customerIdAndAccountNumber);

        Map<String, Object> response = new HashMap<>();
        response.put("account", account);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
