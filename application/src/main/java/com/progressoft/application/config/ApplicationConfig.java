package com.progressoft.application.config;

import event.EventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import repository.AccountRepository;
import usecases.CreateAccountUseCase;
import usecases.DeactivateAccountUseCase;
import usecases.InactivateAccountUseCase;
import validator.AccountValidator;
import validator.CustomerProvider;

@Configuration
public class ApplicationConfig {

    @Bean
    public CreateAccountUseCase createAccountUseCase(AccountRepository accountRepository, AccountValidator accountValidator, EventPublisher eventPublisher) {
        return new CreateAccountUseCase(accountRepository, accountValidator, eventPublisher);
    }

    @Bean
    public DeactivateAccountUseCase deactivateAccountUseCase(AccountRepository accountRepository, EventPublisher eventPublisher) {
        return new DeactivateAccountUseCase(accountRepository, eventPublisher);
    }

    @Bean
    public InactivateAccountUseCase inactivateAccountUseCase(AccountRepository accountRepository, EventPublisher eventPublisher , AccountValidator accountValidator) {
        return new InactivateAccountUseCase(accountRepository, eventPublisher , accountValidator);
    }

    @Bean
    public AccountValidator createAccountValidator(CustomerProvider customerProvider) {
        return new AccountValidator(customerProvider);
    }
}
