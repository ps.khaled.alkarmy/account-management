package usecases;

import event.Event;
import event.EventPublisher;
import lombok.AllArgsConstructor;
import model.Account;
import model.Status;
import repository.AccountRepository;
import validator.AccountValidator;
import validator.Violation;

import java.util.List;

@AllArgsConstructor
public class InactivateAccountUseCase {

    private AccountRepository accountRepository;
    private final EventPublisher eventPublisher;
    private final AccountValidator accountValidator;

    public void execute(Account account) {
        List<Violation> validate = accountValidator.validate(account);
        if (validate.size() != 0) {
            for (Violation violation : validate) {
                throw violation.getException();
            }
        }

        account.setStatus(Status.Active);
        accountRepository.save(account);

        publishEvent(account);
    }

    private void publishEvent(Object payload) {
        eventPublisher.publish(new Event(payload , "ACCOUNT_STATUS_CHANGED"));
    }
}
