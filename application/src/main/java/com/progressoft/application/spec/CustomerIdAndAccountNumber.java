package com.progressoft.application.spec;

import com.progressoft.application.entity.AccountEntity;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;

@And({
        @Spec(path = "customerId", params = "customerId", spec = Equal.class),
        @Spec(path = "accountNumber", params = "accountNumber", spec = Equal.class)
})
public interface CustomerIdAndAccountNumber extends Specification<AccountEntity> {
}
