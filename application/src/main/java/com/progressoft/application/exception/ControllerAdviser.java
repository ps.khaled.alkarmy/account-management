package com.progressoft.application.exception;

import exception.CollectionOfException;
import exception.CustomerNotFoundException;
import exception.InvalidAccountException;
import exception.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class ControllerAdviser extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CollectionOfException.class)
    public ResponseEntity<ErrorResponse> handleAllExceptions(
            CollectionOfException exception
            , WebRequest request) {

        ErrorResponse response = new ErrorResponse();
        response.setUrl(((ServletWebRequest) request).getRequest().getRequestURI());
        List<String> errors = exception.getViolations().stream().map(m -> m.getException().getMessage()).collect(Collectors.toList());

        response.setErrors(errors);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
